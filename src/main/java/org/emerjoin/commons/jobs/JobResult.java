package org.emerjoin.commons.jobs;

public class JobResult<T> {

    private Object value;
    private static final JobResult EMPTY_RESULT = new JobResult(null);

    private JobResult(Object value){
        this.value = value;
    }

    public boolean isEmpty(){
        return value == null;
    }

    public Object getValue(){
        if(value==null)
            throw new IllegalStateException("there is no value present");
        return value;
    }

    public static JobResult<Object> empty(){
        return EMPTY_RESULT;
    }

    public static <T> JobResult<T> of(T object){
        if(object==null)
            throw new IllegalArgumentException("object must not be null");
        return new JobResult<>(object);
    }

}
