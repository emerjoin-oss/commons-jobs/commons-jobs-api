package org.emerjoin.commons.jobs;

public interface JobsProvider {

    void initialize(JobsProviderContext context);
    SchedulerCtl scheduler() throws JobsProviderException;
    boolean supports(Class<? extends Job> jobType) throws JobsProviderException;
    JobSchedule getJobSchedule(Class<? extends Job> jobType) throws JobsProviderException;
    JobExecutionId execute(Class<? extends Job> jobType) throws JobsProviderException;
    JobExecutionId execute(Class<? extends Job> jobType, JobParams params) throws JobsProviderException;
    void schedule(Class<? extends Job> jobType, String expression) throws JobsProviderException;
    void schedule(Class<? extends Job> jobType, String expression, JobParams params) throws JobsProviderException;
    void teardown();

}
