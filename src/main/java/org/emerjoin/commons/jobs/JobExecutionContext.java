package org.emerjoin.commons.jobs;

public interface JobExecutionContext {

    JobExecutionId getExecutionId();
    boolean isInterruptionRequested();

}
