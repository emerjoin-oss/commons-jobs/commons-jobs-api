package org.emerjoin.commons.jobs;

public class JobTypeInfo {

    private JobInfo info;
    private Class<? extends Job> type;
    private Class<? extends JobParamObject> jobParamType;
    private AutoSchedule autoSchedule;

    public JobTypeInfo(Class<? extends Job> type, JobInfo info, AutoSchedule autoSchedule, Class<? extends JobParamObject> jobParamType){
        if(type==null)
            throw new IllegalArgumentException("type must not be null");
        if(info==null)
            throw new IllegalArgumentException("job Info must not be null");
        this.type = type;
        this.autoSchedule = autoSchedule;
        this.jobParamType = jobParamType;
        this.info = info;
    }


    public AutoSchedule getAutoSchedule() {
        return autoSchedule;
    }

    public JobInfo getInfo() {
        return info;
    }

    public Class<? extends JobParamObject> getJobParamType() {
        return jobParamType;
    }

    public Class<? extends Job> getType() {
        return type;
    }

    public boolean isAutoSchedule(){

        return this.autoSchedule != null;

    }

    public boolean isParameterized(){

        return jobParamType!=null;

    }


}
