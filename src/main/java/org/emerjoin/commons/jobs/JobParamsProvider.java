package org.emerjoin.commons.jobs;

public interface JobParamsProvider {

    JobParams provide();

}
