package org.emerjoin.commons.jobs;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@InterceptorBinding
public @interface JobInfo {

    @Nonbinding
    String name();
    @Nonbinding
    String description();
    @Nonbinding
    boolean singleton() default true; //Indicates whether a Job can be executed in parallel or not

}
