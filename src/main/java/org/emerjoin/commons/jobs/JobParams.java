package org.emerjoin.commons.jobs;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class JobParams {

    private Map<String,Object> values = new HashMap<>();

    public static JobParams empty(){
        return new JobParams();
    }

    private JobParams(){

    }

    private JobParams(Map<String,Object> values){
        if(values==null)
            throw new IllegalArgumentException("values must not be null");
        this.values.putAll(values);
    }

    public Map<String,Object> values(){
        return Collections.unmodifiableMap(values);
    }

    public static Builder builder(){
        return new Builder();
    }

    public void put(String key, Object value){
        if(key==null)
            throw new IllegalArgumentException("key must not be null nor empty");
        if(value==null)
            throw new IllegalArgumentException("value must not be null");
        this.values.put(key,value);
    }

    public static JobParams fromMap(Map<String,Object> map){
        if(map==null)
            throw new IllegalArgumentException("map must not be null");
        return new JobParams(map);
    }

    public static class Builder {

        private Map<String,Object> map = new HashMap<>();

        private Builder(){

        }

        public Builder put(Map<String,Object> map){
            if(map==null)
                throw new IllegalArgumentException("map must not be null");
            this.map.putAll(map);
            return this;
        }

        public Builder set(String key, Object value){
            if(key==null||key.isEmpty())
                throw new IllegalArgumentException("key must not be null nor empty");
            if(value==null)
                throw new IllegalArgumentException("value must not be null");
            this.map.put(key,value);
            return this;
        }

        public JobParams build(){
            return new JobParams(map);
        }

    }

}
