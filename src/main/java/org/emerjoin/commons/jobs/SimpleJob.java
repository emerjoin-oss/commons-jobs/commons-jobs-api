package org.emerjoin.commons.jobs;

public interface SimpleJob<R> extends Job {

    JobResult<R> execute() throws JobException;

}
