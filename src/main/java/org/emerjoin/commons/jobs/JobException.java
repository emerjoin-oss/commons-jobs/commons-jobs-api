package org.emerjoin.commons.jobs;

public class JobException extends JobsException {

    public JobException(String message) {
        super(message);
    }

    public JobException(String message, Throwable cause) {
        super(message, cause);
    }

}
