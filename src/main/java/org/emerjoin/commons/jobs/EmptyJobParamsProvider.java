package org.emerjoin.commons.jobs;

import javax.enterprise.context.Dependent;

@Dependent
public class EmptyJobParamsProvider implements JobParamsProvider {

    @Override
    public JobParams provide() {

        return JobParams.empty();

    }

}
