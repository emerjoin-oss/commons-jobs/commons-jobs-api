package org.emerjoin.commons.jobs;

import org.emerjoin.commons.data.mapping.ValueConverter;

public interface JobsBootstrap {

    <FromType,ToType> void registerConverter(Class<FromType> fromType, Class<ToType> toType, ValueConverter<ToType> converter);

}
