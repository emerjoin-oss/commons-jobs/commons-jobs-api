package org.emerjoin.commons.jobs;

public class JobRunRequest {

    private Class<? extends Job> jobType;
    private JobParams jobParams = JobParams.empty();

    public JobRunRequest(Class<? extends Job> jobType){
        this(jobType,JobParams.empty());
    }

    public JobRunRequest(Class<? extends Job> jobType, JobParams params){
        if(jobType==null)
            throw new IllegalArgumentException("jobType must not be null");
        if(params==null)
            throw new IllegalArgumentException("params must not be null");
        this.jobType = jobType;
        this.jobParams = params;
    }

    public Class<? extends Job> getJobType() {
        return jobType;
    }

    public JobParams getJobParams() {
        return jobParams;
    }
}
