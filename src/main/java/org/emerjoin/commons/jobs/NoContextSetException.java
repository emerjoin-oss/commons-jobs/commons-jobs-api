package org.emerjoin.commons.jobs;

public class NoContextSetException extends JobsException {

    public NoContextSetException() {
        super("There is no active JobContext");
    }
}
