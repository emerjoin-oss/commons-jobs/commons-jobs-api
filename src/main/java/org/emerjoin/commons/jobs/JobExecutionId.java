package org.emerjoin.commons.jobs;

public final class JobExecutionId {

    private String value;

    public JobExecutionId(String value){
        if(value==null||value.isEmpty())
            throw new IllegalArgumentException("value must not be null nor empty");
        this.value = value;
    }

    public String value(){
        return this.value;
    }

    public String toString(){
        return String.format("JobExecutionId[%s]",value);
    }


}
