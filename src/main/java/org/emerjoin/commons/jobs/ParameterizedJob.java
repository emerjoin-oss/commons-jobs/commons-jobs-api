package org.emerjoin.commons.jobs;

public interface ParameterizedJob<R,P extends JobParamObject> extends Job, Parameterized {

    JobResult<R> execute(P param) throws JobException;

}
