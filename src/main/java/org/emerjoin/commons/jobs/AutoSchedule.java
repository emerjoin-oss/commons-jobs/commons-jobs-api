package org.emerjoin.commons.jobs;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AutoSchedule {
    String expressionKey();
    String defaultExpression();
    Class<? extends JobParamsProvider> paramsProvider() default EmptyJobParamsProvider.class;
}
