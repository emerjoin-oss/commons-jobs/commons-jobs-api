package org.emerjoin.commons.jobs;

import java.util.Collection;
import java.util.Optional;

public interface JobsContext {

    Collection<JobTypeInfo> listJobs() throws JobsException;
    Collection<JobInstance> listRunning() throws JobsException;
    Collection<JobInstance> listRunning(Class<? extends Job> jobType) throws JobsException;
    void rescheduleAll() throws JobsException;
    void reschedule(Class<? extends Job> jobType) throws JobsException;
    void execute(Class<? extends Job> jobType, JobParams params) throws JobsException;
    void execute(Class<? extends Job> jobType) throws JobsException;
    JobExecutionId trigger(Class<? extends Job> jobType, JobParams params) throws JobsException;
    JobExecutionId trigger(Class<? extends Job> jobType) throws JobsException;
    Optional<JobTypeInfo> getJobTypeInfo(Class<? extends Job> jobTyp) throws JobsException;
    Optional<JobTypeInfo> getJobTypeInfoByName(String name) throws JobsException;

}
