package org.emerjoin.commons.jobs;

import java.util.Map;

public interface JobsProviderContext {

    void executeScheduled(Class<? extends Job> jobType, JobExecutionId executionId);
    void execute(Class<? extends Job> jobType, JobExecutionId executionId);
    void execute(Class<? extends Job> jobType, JobExecutionId executionId, Map<String,Object> params);
    JobTypeInfo getTypeInfo(Class<? extends Job> jobType);

}
