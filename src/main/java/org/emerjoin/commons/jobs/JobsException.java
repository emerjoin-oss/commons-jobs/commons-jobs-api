package org.emerjoin.commons.jobs;

public class JobsException extends RuntimeException {

    public JobsException(String message){
        super(message);
    }

    public JobsException(String message, Throwable cause){
        super(message,cause);
    }

}
