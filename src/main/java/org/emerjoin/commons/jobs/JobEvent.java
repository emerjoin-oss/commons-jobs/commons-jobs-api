package org.emerjoin.commons.jobs;

public class JobEvent {

    private JobExecutionId jobExecutionId;
    private Class<? extends Job> jobType;

    public JobEvent(Class<? extends Job> jobType, JobExecutionId jobExecutionId){
        if(jobType==null)
            throw new IllegalArgumentException("jobType must not be null nor empty");
        if(jobExecutionId==null)
            throw new IllegalArgumentException("jobExecutionId must not be null");
        this.jobType = jobType;
        this.jobExecutionId = jobExecutionId;
    }

    public Class<? extends Job> getJobType() {
        return jobType;
    }

    public JobExecutionId getJobExecutionId() {
        return jobExecutionId;
    }
}
