package org.emerjoin.commons.jobs;

public interface JobsController {

    void bootstrap();
    void teardown();

}
