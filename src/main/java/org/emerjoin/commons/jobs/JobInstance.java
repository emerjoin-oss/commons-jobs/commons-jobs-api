package org.emerjoin.commons.jobs;

public interface JobInstance {

    JobExecutionId getId();
    JobTypeInfo getTypeInfo();
    boolean isInterruptionRequested();
    void interrupt();
    boolean isRunning();

}
