package org.emerjoin.commons.jobs;

public class BeforeJobExecution extends JobEvent {

    private boolean halt = false;
    private JobParams jobParams;

    public BeforeJobExecution(Class<? extends Job> jobType, JobExecutionId jobExecutionId){
        this(jobType,jobExecutionId,JobParams.empty());
    }

    public BeforeJobExecution(Class<? extends Job> jobType, JobExecutionId jobExecutionId, JobParams jobParams) {
        super(jobType, jobExecutionId);
        this.jobParams = jobParams;
    }

    public void halt(){
        this.halt = true;
    }

    public boolean isHalt() {
        return halt;
    }

    public JobParams getJobParams() {
        return jobParams;
    }
}
