package org.emerjoin.commons.jobs;

import java.util.Optional;

public class AfterJobExecution extends JobEvent {

    private RuntimeException exception;
    private JobResult jobResult;

    public AfterJobExecution(Class<? extends Job> jobType, JobExecutionId jobExecutionId) {
        super(jobType, jobExecutionId);
    }

    public AfterJobExecution(Class<? extends Job> jobType, JobExecutionId jobExecutionId, JobResult jobResult) {
        super(jobType, jobExecutionId);
        if(jobResult==null)
            throw new IllegalArgumentException("jobResult must not be null");
        this.jobResult = jobResult;
    }

    public AfterJobExecution(Class<? extends Job> jobType, JobExecutionId jobExecutionId, RuntimeException exception) {
        super(jobType, jobExecutionId);
        if(exception==null)
            throw new IllegalArgumentException("exception must not be null");
        this.exception = exception;
    }

    public JobResult getJobResult() {
        return jobResult;
    }

    public boolean hasFailed(){
        return exception!=null;
    }

    public Optional<RuntimeException> getException() {
        return Optional.ofNullable(
                exception);
    }
}
