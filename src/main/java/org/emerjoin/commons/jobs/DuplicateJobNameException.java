package org.emerjoin.commons.jobs;

public class DuplicateJobNameException extends JobsException {

    public DuplicateJobNameException(String name) {
        super(String.format("The Job name [%s] is duplicate",name));
    }

}
