package org.emerjoin.commons.jobs;

public interface SchedulerCtl {

    void standby();
    boolean isInStandby();
    void resume();

}
