package org.emerjoin.commons.jobs;

public class NoProviderSetException extends JobsException {

    public NoProviderSetException() {
        super("There is no Jobs provider set");
    }
}
