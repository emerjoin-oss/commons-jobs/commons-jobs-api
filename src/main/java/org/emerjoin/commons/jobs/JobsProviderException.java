package org.emerjoin.commons.jobs;

public class JobsProviderException extends JobsException {

    public JobsProviderException(String message) {
        super(message);
    }

    public JobsProviderException(String message, Throwable cause) {
        super(message, cause);
    }

}
