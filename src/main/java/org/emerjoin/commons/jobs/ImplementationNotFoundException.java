package org.emerjoin.commons.jobs;

public class ImplementationNotFoundException extends JobsException {

    public ImplementationNotFoundException() {
        super("jobs-api Implementation not found");
    }
}
