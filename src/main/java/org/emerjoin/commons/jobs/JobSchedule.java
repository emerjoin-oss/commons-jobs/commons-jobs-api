package org.emerjoin.commons.jobs;

public interface JobSchedule {

    void change(String expression) throws JobsProviderException;

}
