# Maven Coordinates
## Repository
Emerjoin OSS repository
```xml
<repository>
    <id>emerjoin-oss</id>
    <name>maven</name>
    <url>https://pkg.emerjoin.org/oss</url>
</repository>
```

## Artifact
Latest version
```xml
<dependency>
    <groupId>org.emerjoin.commons</groupId>
    <artifactId>commons-jobs-api</artifactId>
    <version>1.0.0-final</version>
</dependency>
```

# Features
This API is all about developer experience and it is driven by the following requirements:
* Support different job scenarios:
    * Jobs without parameters
    * Jobs with parameters
    * Jobs with state (coming soon)
    * Jobs with state and parameters (coming soon)
* Concurrency control
    * Singleton Jobs - single running instance jobs
    * Non Singleton Jobs - jobs that support parallel running instances
* Support different schedulers
* Automated scheduling of jobs
* Dynamic scheduling - schedule the job and then change it's execution schedule in runtime
* Easy testability of Jobs (Unit testing)
* CDI Native
    * Jobs as CDI Beans
    * CDI Events

# Usage

## Creating a simple Job
A stateless Job that doesn't take any parameter:
```java
    @JobInfo(name="simple-job",description="Lorem ipsum dolor")
    public class SalesReportJob<T> implements SimpleJob {
    
        @Override
        public JobResult<Object> execute(){
            //TODO: your thing
        }

    }
```

## Creating a parameterized Job
A stateless Job that takes a parameter:
```java
    
    @JobInfo(name="parameterized-job",description="Lorem ipsum dolor")
    public class ParameterizedSalesReportJob<T> implements ParameterizedJob<Object,SalesReportPeriod> {
    
        @Override
        public JobResult<Object> execute(SalesReportPeriod param){
            
            //TODO: your thing here

        }

    }
    
```
### SalesReportPeriod
Job Params are passed in a Map.
This object is type-safe proxy used to read values from a Map:
```java
    interface SalesReportPeriod extends JobParamObject {
        
        @Property("period.year")
        int getYear();

        @Property("period.month")
        int getMonth();

    }
```
The **@Property** annotation defines the key to be used to read the value from the **JobParams** map.
Read more about this mechanism on the [commons-data-mapping documentation](https://gitlab.com/emerjoin-oss/commons-data-mapping)

## Automatically scheduling a Job
Just annotate your job class with **AutoSchedule** as follows:

### Define Cron expression Key
The Key will be resolved using deltaspike **ConfigResolver**;
```java
    @JobInfo(..)
    @AutoSchedule(expressionKey="MY_JOB_EXPR_KEY")
    public class MyJobClass implements WhateverJobType {
        
    }
```


### Define Cron expression Key with Default Value
You can as well define a default expression to be used whenever the **ExpressionsProvider** can't provide a value for the **expressionKey**.
```java
    @JobInfo(..)
    @AutoSchedule(expressionKey="MY_JOB_EXPR_KEY", defaultExpression="0 15 10 ? * MON-FRI")
    public class MyJobClass implements WhateverJobType {
        
    }
```

## Job Params provider
Used to provide params to a Job when triggered by a schedule expression:

### Defining the provider
The provider must be a managed bean that implements the **JobParamsProvider** interface:
```java
    @Dependent
    public MyJobParamsProvider implements JobParamsProvider {
        
        public JobParams provide(){
            //TODO: Your thing
        }   

    }
```

### Referencing the provider
The provider is referenced on the **AutoSchedule** annotation
```java
    @JobInfo(...)
    @AutoSchedule(...paramsProvider=MyJobParamsProvider.class)
    public class MyJobClass implements WhateverJobType {
        
    }
```

## Concurrency control
You can choose to have a singleton job or a non-singleton one. 
Being singleton means that the Jobs Scheduler will not allow concurrent execution of that Job.
All Jobs are singleton by default.

To mark a Job as non-singleton you have to switch off the **singleton** flag on the **JobInfo** annotation:
```java
    @JobInfo(...singleton = false)
```

## CDI Features

### Scopes
* @JobExecutionScoped - a context that is only active during a Job Execution and only within the Thread on which the Job is being executed.

### Events
* JobsBootstrap - fired when bootstrapping the API. Can be used to configure the API
* BeforeJobExecution - fired when a Job is about to be executed
* AfterJobExecution - fired when the Job finished execution
* JobRunRequest - event that triggers the execution of a Job 

### Injectable beans
* JobsContext - this is the entry point of the API. This bean is @ApplicationScoped
* JobExecutionContext - this is used by Jobs to communicate with running Job instance. This bean is @JobExecutionScoped
* SchedulerCtl - this is used to control the Jobs Scheduler. This bean is @ApplicationScoped

## Unit testing jobs
You must use **JobsContext** to run the Job, instead of Injecting the Job directly. Check the JUnit same below:
```java
...

@Inject
JobsContext context;

@Test
public void parameterized_job_test(){
    //Prepare parameters
    JobParams params = JobParams.builder()
        .set("my.param.key","value here")
        .set("another.param.key","another value here")
        .build();
    //Execute the Job
    context.execute(MyJobClass.class,params);
}   


@Test
public void simple_job_test(){
    
    context.execute(MyJobClass.class);

}   
...
```